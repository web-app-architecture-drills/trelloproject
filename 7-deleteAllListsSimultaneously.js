const apiKey = "7b9880e2b9405997b85593c3f15791a0";
const tokenKey = "ATTA2e8c52faab815ecb732444749d046569368b3e9e26d01a43d7d0ce9f9154ac5bC67E8366";
const createNewBoard = require("./6-createNewBoard");

function deleteAllListAtOnce(){
    
    return new Promise((resolve, reject) => {
        createNewBoard('newBoard')
        .then( (data) => {
            let idArray = [];
            idArray = data.map((index) => index.idList)

            promiseArray = idArray.map( (eachId) => {
                return fetch(`https://api.trello.com/1/lists/${eachId}/closed?value=true&key=${apiKey}&token=${tokenKey}`, {
                    method: 'PUT'
                })
            })
            Promise.all(promiseArray)
            .then((data) => {
                // resolve(data);
                resolve("All lists deleted successfully");
            })
            .catch((error) => {
                reject(error);
            })
        })
    })
}

module.exports = deleteAllListAtOnce;