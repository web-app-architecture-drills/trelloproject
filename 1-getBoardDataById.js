const apiKey = "7b9880e2b9405997b85593c3f15791a0";
const tokenKey = "ATTA2e8c52faab815ecb732444749d046569368b3e9e26d01a43d7d0ce9f9154ac5bC67E8366";

function getBoard(boardId){
    return new Promise((resolve ,reject) => {
        fetch(`https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${tokenKey}`)
        .then((response) => response.json())
        .then((data) => resolve(data))
        .catch((err) => {
            reject(err);
        })
    })
}

module.exports = {getBoard};
