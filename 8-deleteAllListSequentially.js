const apiKey = "7b9880e2b9405997b85593c3f15791a0";
const tokenKey = "ATTA2e8c52faab815ecb732444749d046569368b3e9e26d01a43d7d0ce9f9154ac5bC67E8366";
const createNewBoard = require("./6-createNewBoard");

function deleteList(id){
    return new Promise((resolve , reject)=>{
        fetch(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${apiKey}&token=${tokenKey}`, {
                    method: 'PUT'
        })
        .then(()=>{
            resolve(`${id} file deleted`)
        })
        .catch((err)=>{
            reject(err)
        })
    })
}

function deleteAllListSequentially(){

    return new Promise((resolve, reject) => {
        createNewBoard('Board-8')
        .then( (data) => {
            let idArray = [];
            idArray = data.map((index) => index.idList)

            for(let index=0; index<idArray.length; index++){
                deleteList(idArray[index])
                .then(data => console.log(data))
            }
        })
        .catch((err) => {
            reject(err);
        })
    })
}

module.exports = deleteAllListSequentially;