const apiKey = "7b9880e2b9405997b85593c3f15791a0";
const tokenKey = "ATTA2e8c52faab815ecb732444749d046569368b3e9e26d01a43d7d0ce9f9154ac5bC67E8366";
const getCards = require('./4-getCardByListId')

function getAllCards(boardId){
    return new Promise((resolve, reject) => {

        fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${tokenKey}`, {
            method: 'GET'
        })
        .then((response) => response.json())
        .then((data) => {
            let idList = data.map((index) => index.id)
            return idList;
        })
        .then((idList) => {
            let promiseArray = [];
            promiseArray = idList.map((eachId) => getCards(eachId))
            return Promise.all(promiseArray)
        })
        .then((data) => resolve(data))
        .catch((err) => {
            reject(err);
        })
    })
}

module.exports = getAllCards;