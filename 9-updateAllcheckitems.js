const apiKey = "7b9880e2b9405997b85593c3f15791a0";
const tokenKey = "ATTA2e8c52faab815ecb732444749d046569368b3e9e26d01a43d7d0ce9f9154ac5bC67E8366";

function getCheckItemsIds(checkListId){
    return new Promise( (resolve, reject) => {
    fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${tokenKey}`, {
        method: 'GET'
    })
    .then(response => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then(text => resolve(text))
    .catch(err => console.error(err));
    })
}

function updateCheckItems(cardId, checkListId){
    return new Promise( (resolve, reject) => {
        getCheckItemsIds(checkListId)
        .then((checkListId) => {

            checkListId.map((value) => {

                fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${value.id}?state=${'complete'}&key=${apiKey}&token=${tokenKey}`, {
                    method: 'PUT'
                })
                .then(response => {
                    console.log(`Response: ${response.status} ${response.statusText}`);
                    return response.json();
                })
                .then(text => console.log(text))
                .catch(err => console.error(err));            
            })
        })
    })
}

module.exports = updateCheckItems;