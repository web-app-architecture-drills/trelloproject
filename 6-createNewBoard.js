const apiKey = "7b9880e2b9405997b85593c3f15791a0";
const tokenKey = "ATTA2e8c52faab815ecb732444749d046569368b3e9e26d01a43d7d0ce9f9154ac5bC67E8366";
const createBoard = require('./2-getBoardDataByName')

function create3Lists(id, name){
    return new Promise( (resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${apiKey}&token=${tokenKey}`, {
            method: 'POST',
        })
        .then((response) => response.json())
        .then((data) => resolve(data))
    })
}

function createCard(dataId, cardName){
    return new Promise( (resolve, reject) => {
        fetch(`https://api.trello.com/1/cards?idList=${dataId}&name=${cardName}&key=${apiKey}&token=${tokenKey}`, {
            method: 'POST',
        })
        .then((response) => response.json())
        .then((data) => resolve(data))        
    })
}

function createNewBoard(newBoard){
    return new Promise( (resolve, reject) => {
        createBoard(newBoard)
        .then(boardData => {
            let dataID = boardData.id;
            let dataName = boardData.name;
            let promiseArray = [];
            
            for(let index=0; index<3; index++){
                promiseArray.push(
                    create3Lists(dataID, dataName+(index+1))
                    .then((listData) => createCard(listData.id, "Card"+1))
                );
            }
            Promise.all(promiseArray)
            .then( (data) => {
                resolve(data);
            })
        })
        .catch((err) => {
            reject(err);
        })
    })
}

module.exports = createNewBoard;